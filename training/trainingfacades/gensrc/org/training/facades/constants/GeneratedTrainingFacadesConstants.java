/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 19-Mar-2022, 6:33:01 PM                     ---
 * ----------------------------------------------------------------
 */
package org.training.facades.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated(since = "ages", forRemoval = false)
@SuppressWarnings({"unused","cast"})
public class GeneratedTrainingFacadesConstants
{
	public static final String EXTENSIONNAME = "trainingfacades";
	
	protected GeneratedTrainingFacadesConstants()
	{
		// private constructor
	}
	
	
}
